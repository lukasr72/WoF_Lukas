/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Itemy;

import java.util.ArrayList;

/**
 *
 * @author Lukáš
 */
public class Index extends Item {
    private ArrayList<String> znamky;
    
    
    public Index(String nazov, String popis, int cena) {
        super(nazov, popis, cena, SlotyVybavy.NIC);     
        this.znamky = new ArrayList<String>();
    }
    
    public void zapisZnamku(String znamka) {       
        this.znamky.add(znamka);
    }
    
    public double getProspech() {
        int pocetZnamok = 0;
        double sucetZnamok = 0;        
        
        for (String znamka : this.znamky) {
            if (znamka == "A") {
                pocetZnamok++;
                sucetZnamok += 1;
            }
            if (znamka == "B") {
                pocetZnamok++;
                sucetZnamok += 1.5;
            }
            if (znamka == "C") {
                pocetZnamok++;
                sucetZnamok += 2;
            }
            if (znamka == "D") {
                pocetZnamok++;
                sucetZnamok += 2.5;
            }
            if (znamka == "E") {
                pocetZnamok++;
                sucetZnamok += 3;
            }
            if (znamka == "Fx") {
                pocetZnamok++;
                sucetZnamok += 4;
            }                
        }                
        return (sucetZnamok / pocetZnamok);
    }   
    
    public void zmazZnamky() {
        this.znamky.clear();
    }
}
