/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Itemy;

import Hra.IPrikaz;
import Hra.Prikaz;
import Hrac.Hrac;

/**
 *
 * @author roman17
 */
public class Navleky extends Item implements IPrikaz {

    public Navleky(String nazov, String popis, int cena) {
        super(nazov, popis, cena, SlotyVybavy.NAVLEKY);
    }
    
    

    @Override
    public boolean pouzi(Prikaz prikaz, Hrac hrac) {
        hrac.getInventar().oblecItem(this);
        return true;
    }

    @Override
    public boolean jePrikaz(String nazov) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void vypisPrikazy() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
}
