/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package NPC;

import Hra.Prikaz;
import Hrac.Hrac;
import Itemy.ISIC;
import Itemy.Index;
import Itemy.Item;
import java.util.Random;

/**
 *
 * @author Lukáš
 */

//ma vediet: zapisat 7 znamok - nahodne znamky

public class Profesor extends NPC {
    
    public Profesor(String nazov, String privitanie) {
        super(nazov, privitanie);
    }
    //ked mam uz znamky, nezapise dalsie
    @Override
    public boolean pouziPrikazNPC(Prikaz prikaz, Hrac hrac) {
        switch (prikaz.getNazov()) {
            case "zapisatZnamky": {
                Item temp = hrac.getInventar().getItem("Index");
                if (temp instanceof Index) {
                    Index index = (Index)temp;   
                    if (index.getProspech() > 0) {
                        System.out.println("Nejaké známky už zapisané máš. Nemôžem zapisať ďalšie...");
                        return true;
                    }
                    for (int k = 0; k < 7; ++k) {
                        index.zapisZnamku(this.getZnamka());
                    }        
                    System.out.println("Známky zapisane :) ");
                    return true;
                }                
                System.out.println("Nemaš index, chod na študijné");
                return true;                
                
            }            
            default:
                return super.pouziPrikazNPC(prikaz, hrac);
        }
    }

    @Override
    public boolean jePrikazNPC(String nazov) {
        switch (nazov) {
            case "zapisatZnamky":
                return true;            
            default:
                return super.jePrikazNPC(nazov);
        }
    }

    @Override
    public void vypisPrikazyNPC() {
        System.out.print("zapisatZnamky ");
        super.vypisPrikazyNPC();
    }

    private String getZnamka() {
        String znamka = "";
        Random r = new Random();
        
        int value = r.nextInt(6);
        switch (value) {
            case 0: znamka = "A"; break;               
            case 1: znamka = "B"; break;
            case 2: znamka = "C"; break;
            case 3: znamka = "D"; break;
            case 4: znamka = "E"; break;
            case 5: znamka = "Fx"; break;
        }                
        return znamka;
    }
    
}
