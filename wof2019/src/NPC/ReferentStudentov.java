/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package NPC;

import Hra.Prikaz;
import Hrac.Hrac;
import Itemy.ISIC;
import Itemy.Index;
import Itemy.Item;
import java.util.Random;

/**
 *
 * @author Lukáš
 */

//bude vediet: vytvorit index pre hraca; prijať ziadosť na stipendium; udelit stipendium > prida kredit na ISIC

public class ReferentStudentov extends NPC {
    private boolean stipendium;   
    
    public ReferentStudentov(String nazov, String privitanie) {
        super(nazov, privitanie);
        this.stipendium = false; //narok na stipendium
    }
    
    
    
    @Override
    public boolean pouziPrikazNPC(Prikaz prikaz, Hrac hrac) {
        switch (prikaz.getNazov()) {
            case "vydajIndex": {                
                //vytvori danemu hracovi item Index
                if (hrac.getInventar().getItem("Index") == null) {
                    Index index = new Index ("Index", "Tu si daj zapísať známky", 0);
                    hrac.getInventar().vlozItem(index);
                    System.out.println("Teraz máš index a bež si zapísať známky a vrať sa po štipendium.");
                    return true;
                }
                System.out.println("Zdá sa, že index už máš.");
                return true;                
            }
            
            case "odovzdajIndex": {
                //ma alebo nema narok na stipendium
                if (hrac.getInventar().getItem("Index") == null) {
                    System.out.println("Zatiaľ nemáš index.");
                    return true;
                }
                
                Index index = (Index)hrac.getInventar().getItem("Index");
                if (index.getProspech() == 0) {
                    System.out.println("Nemáš žiadne známky.");
                    return true;
                }
                System.out.println("Tvoj prospech: " + index.getProspech());
                if (index.getProspech() < 2.85) {
                    this.stipendium = true;
                    System.out.println("Máš nárok na štipendium. Napíš 'chcemStipendium'");                    
                } else {
                    System.out.println("Učil si sa slabo. Štipendium nebude :/ ");                    
                }
                return true;
            }
            
            case "chcemStipendium": {
                int vyskaStipendia = this.getNahodneStipendium();                           
                if (this.stipendium) {
                    Item temp = hrac.getInventar().getItem("ISIC");
                    if (temp instanceof ISIC) {
                        ISIC isic = (ISIC)temp;
                        isic.modifikujKredit(vyskaStipendia);
                        System.out.println("Získavaš štipendium!!!");
                        System.out.println("Si bohatší o " + vyskaStipendia + " €");
                        Index index = (Index) hrac.getInventar().getItem("Index");
                        index.zmazZnamky();
                        this.stipendium = false;
                        return true;
                    }
                    System.out.println("Nemáš ISIC");                    
                    return true;
                }
                System.out.println("Neodovzdal si index, slabo si sa učil alebo si už štipendium dostal, skús o rok :) ");
                return true;
            }
            default:
                return super.pouziPrikazNPC(prikaz, hrac);
        }
    }
    
    
    @Override
    public boolean jePrikazNPC(String nazov) {
        switch (nazov) {
            case "vydajIndex":
                return true;
            case "odovzdajIndex":
                return true;
            case "chcemStipendium":
                return true;
            default:
                return super.jePrikazNPC(nazov);
        }
    }

    @Override
    public void vypisPrikazyNPC() {
        System.out.print("vydajIndex odovzdajIndex chcemStipendium ");
        super.vypisPrikazyNPC();
    }
    
    private int getNahodneStipendium() {
        Random r = new Random();
        int value = r.nextInt(1500-500) + 500;
        return value;
    }
    
}
