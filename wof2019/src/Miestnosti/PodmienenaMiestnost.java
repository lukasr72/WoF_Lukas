/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Miestnosti;

import Dvere.IDvere;
import Hrac.Hrac;
import Itemy.Item;
import NPC.NPC;
import java.util.Collection;
import java.util.HashMap;

/**
 *
 * @author roman17
 */
public class PodmienenaMiestnost {
    private String nazovMiestnosti;
    private String popisMiestnosti;
    private HashMap<String, IDvere> vychody;
    private HashMap<String, Item> itemy;
    private HashMap<String, NPC> npc;
    
    public PodmienenaMiestnost(String nazov, String popis) {
        this.nazovMiestnosti = nazov;
        this.popisMiestnosti = popis;
        this.vychody = new HashMap<>();
        this.itemy = null;
        this.npc = new HashMap<String, NPC>();
    }
    
    public void nastavVychod(IDvere dvere) {
        this.vychody.put(dvere.dajDruhuMiestnost(this).nazovMiestnosti, dvere);
    }

    /**
     * @return textovy popis miestnosti.
     */
    public String getPopis() {
        return this.popisMiestnosti;
    }

    public String getNazov() {
        return this.nazovMiestnosti;
    }
    
    public void vypisVychody() {
        for (String key: vychody.keySet()) {
            System.out.print(key + " ");
        }      
    }
    
    public IDvere getDvere(String nazovDveri) {
        return this.vychody.get(nazovDveri);
    }

    public Miestnost getMiestnost(String nazovMiestnosti) {
        return this.vychody.get(nazovMiestnosti).dajDruhuMiestnost(this);
    }
    
    public void pridajItemy(HashMap<String, Item> itemy) {
        this.itemy = itemy;
    }
    
    public void vypisItemy() {
        if (this.itemy == null)
            return;
        for (Item item : this.itemy.values()) {
            System.out.print(item.getNazov() + " ");
        }
        System.out.println("");
    }
    
    public Item zoberItem(String nazov) {
        return this.itemy != null ? this.itemy.remove(nazov) : null;
    }
    
    public Collection<IDvere> getVsetkyDvere() {
        return this.vychody.values();
    }
    
    public void pridajNPC(NPC npc) {
        this.npc.put(npc.getNazov(), npc);
    }
    
    public NPC getNPC(String meno) {
        return this.npc.get(meno);
    }
    
    public Collection<NPC> getVsetkyNPC() {
        return this.npc.values();
    }

    public void vypisNPC() {
        System.out.print("NPC: ");
        for (String npc : this.npc.keySet()) {
            System.out.print(npc + " ");
        }
        System.out.println("");
    }
    
    public boolean skusVojst(Hrac hrac) {
        
        return true;
    }
    
    public String getPodmienkuVstupu() {
        return "";
        //vytvorit navleky, podmienenu miestnost, podmienky ci su navleky, labak>podmienena miestnost 
    }
}
